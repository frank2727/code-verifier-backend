// Environment Variables
import dotenv from "dotenv";
import server from './src/server';
import { LogError, LogSuccess } from "./src/utils/logger";

// Configuration the .env file
dotenv.config();

const port: string | number = process.env.PORT || 8000;

// Execute SERVER
server.listen(port, () => {
  LogSuccess(`Server is running on port $http://localhost:${port}/api`)
})

// Control SERVER ERROR
server.on('error', (error) => {
  LogError(`Server Error: ${error}`)
})