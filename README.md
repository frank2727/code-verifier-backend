# Las dependencias que se han instalado en el proyecto y para que sirven

## Dependencias

* **cors**: Es una herramienta que nos permite realizar peticiones HTTP desde un dominio diferente al del servidor.

* **helmet**: Ayuda a proteger la aplicación de algunas vulnerabilidades web conocidas mediante el establecimiento correcto de cabeceras HTTP.

* **dotenv**: Es una herramienta que nos permite leer variables de entorno desde un archivo.

* **express**: Es un framework de nodejs que nos permite crear servidores web de una manera muy sencilla.

* **mongoose**: Es el ORM que vamos a utilizar para escribir consultas para una base de datos de MongooDB.

## Dependencias de desarrollo

* **typescript**: Es un lenguaje de programación que compila a JavaScript. Nos permite escribir código en un lenguaje más moderno y que nos permite tener más control sobre el código.

* **ts-node**: Nos permite ejecutar código TypeScript sin necesidad de compilarlo.

* **ts-jest**: Es una herramienta que nos permite ejecutar tests de TypeScript.

* **nodemon**: Es una herramienta que nos permite reiniciar el servidor automáticamente cada vez que se detecta un cambio en el código.

* **eslint**: Es una herramienta que nos permite detectar errores en el código.

* **serve**: Es una herramienta que nos permite ejecutar un servidor web estático.

* **@types/express**: Los paquetes de Express y TypeScript son independientes. Consecuentemente, TypeScript no reconoce los tipos de las clases de Express. Existe un paquete npm concreto para que TypeScript reconozca los tipos de Express.

* **@types/node**: Es un paquete que nos permite usar las funciones de Node.js en TypeScript.

* **@types/jest**: Es un paquete que nos permite usar las funciones de Jest en TypeScript.

* **jest**: Es una herramienta que nos permite ejecutar tests.

* **supertest**: Es una herramienta que nos permite ejecutar tests de integración.

* **concurrently**: Es una herramienta que nos permite ejecutar scripts de npm en paralelo.

* **webpack**: Es una herramienta que nos permite empaquetar el código de nuestra aplicación.

* **webpack-cli**: Es una herramienta que nos permite ejecutar webpack desde la línea de comandos.

* **webpack-node-externals**: Es una herramienta que nos permite excluir paquetes de node_modules de nuestro bundle.

* **webpack-shell-plugin**: Es una herramienta que nos permite ejecutar comandos de shell antes o después que se compile webpack.

## Scripts

* **build**: Compila el código TypeScript a JavaScript.

* **start**: Ejecuta el código JavaScript compilado.

* **dev**: Ejecuta los comandos de `npx tsc --watch` y `nodemon -q dist/index.js` en paralelo.

* **test**: Ejecuta los tests.

* **serve:coverage**: Ejecuta un servidor web estático con el reporte de cobertura de tests.

## Variables de entorno

* **PORT**: Puerto en el que se ejecuta el servidor.