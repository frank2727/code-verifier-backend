"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const dotenv_1 = require("dotenv");
// Configuration the .env file
dotenv_1.default.config();
// Create Express App
const app = (0, express_1.default)();
const port = process.env.PORT || 8000;
// Define the first route of App
app.get("/", (req, res) => {
    // Send Hello World
    res.send("Welcome to API Restful: Express + TS + Swagger + Mongoose");
});
// Execute App and Listen Requests to port
app.listen(port, () => console.log(`Running at http://localhost:${port}`));
