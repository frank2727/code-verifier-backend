import express, { Request, Response } from "express"
import { GoodByeController } from "../controller/GoodByeController"
import { LogInfo } from "../utils/logger"

const goodbyeRouter = express.Router();

// http://localhost:8000/api/goodbye?name=Frank/

goodbyeRouter.route("/")
    .get(async (req: Request, res: Response) => {
        // Obtain a Query Param
        const { name }: any = req.query;
        LogInfo(`Query Param: ${name}`);
        // Controller Instance to execute method
        const controller : GoodByeController = new GoodByeController();
        // Obtain Response
        const response = await controller.getMessage(name);
        // Send to the client the response
        return res.send(response);
    })

// Export GoodBye Router
export default goodbyeRouter;