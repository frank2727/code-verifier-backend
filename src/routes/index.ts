/***
 * Root Router
 * Redirections to Routers
 */

import express, { Express, Request, Response } from "express";
import helloRouter from "./HelloRouter";
import goodbyeRouter from "./GoodByeRouter";
import { LogInfo } from "../utils/logger";

// Server instance
const server: Express = express();

// Router instance
const rootRouter = express.Router();

// Activate for requests to http://localhost:3000/api

// GET: http://localhost:3000/api/
rootRouter.get('/', (req: Request, res: Response) => {
    LogInfo('GET: http://localhost:3000/api/');
    // Send Hello World!
    res.send('Hello World');
  })

// Redirections to Routers & Controllers
server.use("/", rootRouter); // http://localhost:8000/api/
server.use("/hello", helloRouter); // http://localhost:8000/api/hello --> HelloRouter
server.use("/goodbye", goodbyeRouter); // http://localhost:8000/api/goodbye --> GoodByeRouter
// Add more routes here...


export default server;