import { GoodByeResponse } from "./types";
import { IGoodByeController } from "./interfaces";
import { LogSuccess } from "../utils/logger";
import { Get, Query, Tags, Route } from "tsoa";

@Route("/api/goodbye")
@Tags("GoodByeController")
export class GoodByeController implements IGoodByeController {
  /**
   * Endpoint to retreive a Message "GoodBye {name}" in JSON
   * @param { string | undefined } name Name of user to be greeted
   * @returns { BasicResponse } Promise of BasicResponse
   */
  @Get("/")
  public async getMessage(@Query()name?: string): Promise<GoodByeResponse> {
    LogSuccess("[/api/goodbye] Get Request");

    return {
      message: `GoodBye ${name || "anónimo"}`,
      date: String(new Date()),
    };
  }
}
