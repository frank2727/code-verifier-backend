/**
 * Basic JSON response for Controllers
 */

export type BasicResponse = {
  message: string;
};

/**
 * Error JSON response for Controllers
 */

export type ErrorResponse = {
  error: string;
  message: string;
};

/***
 * GoodBye JSON response for Controllers
 */

export type GoodByeResponse = {
  message: string;
  date: string;
};
