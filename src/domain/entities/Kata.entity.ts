import mongoose from "mongoose";

export const kataEntity = () => {
  const KataSchema = new mongoose.Schema({
    name: {
      type: String,
      required: true,
    },
    description: {
        type: String,
        required: true,
    },
    level: {
        type:Number,
        required: true,
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    date: {
        type: Date,
        required: true
    },
    valoration: {
        type: Number,
        required: true
    },
    chances: {
        type: Number,
        required: true
    }
  })

  return mongoose.model('Katas', KataSchema);
};
