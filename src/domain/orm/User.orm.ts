import { userEntity } from "../entities/User.entity";
import { LogError, LogSuccess } from "@/utils/logger";

// CRUD Operations

/**
 * Method to obtain all Users from Collection "Users" in MongoDB
 * @returns 
 */
export const GetAllUsers = async (): Promise<any[] | undefined> => {
  try {
    const userModel = userEntity()
    
    // Search all users
    return await userModel.find({isDeleted: false});
  } catch (error) {
    LogError(`[ORM ERROR]: Getting All Users: ${error}`);
    throw error;
  }
};

// TODO:
// Get User By ID
// Create New User
// Update User
// Delete User By ID
// Update User By ID
