import { LogError, LogSuccess } from "@/utils/logger";
import { kataEntity } from "../entities/Kata.entity";

// CRUD Operations

/**
 * Method to obtain all Katas from Collection "Katas" in MongoDB
 * @returns
 */
export const GetAllKatas = async () => {
    try {
        const kataModel = kataEntity()

        // Search all Katas
        await kataModel.find({isDeleted: false})
    } catch (error) {
        LogError(`[ORM ERROR]: Getting All Katas: ${error}`)
    }
}